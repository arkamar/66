/* 
 * sv_alltype_zero.c
 * 
 * Copyright (c) 2018-2019 Eric Vidal <eric@obarun.org>
 * 
 * All rights reserved.
 * 
 * This file is part of Obarun. It is subject to the license terms in
 * the LICENSE file found in the top-level directory of this
 * distribution.
 * This file may not be copied, modified, propagated, or distributed
 * except according to the terms contained in the LICENSE file./
 */

#include <66/parser.h>
#include <66/resolve.h>

sv_alltype const sv_alltype_zero = SV_ALLTYPE_ZERO ;
sv_name_t const sv_name_zero = SV_NAME_ZERO ;
keynocheck const keynocheck_zero = KEYNOCHECK_ZERO ;
ss_resolve_t const ss_resolve_zero = RESOLVE_ZERO ;
